# LED Spicer configuration

Please be sure to update `ledspicer.conf` to include the appropriate pins to your (96) buttons.  When wiring the buttons to the board, I took a "whatever fits" approach since I was able to set everything with software.  Presumably, you have done the same.

