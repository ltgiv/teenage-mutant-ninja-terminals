#!/usr/bin/env bash
: <<'!COMMENT'
#################################################
#         Teenage Mutant Ninja Terminals        #
#            Thad.Getterman.org/tmnt            #
#################################################
!COMMENT

################################################################################
SOURCE="${BASH_SOURCE[0]}" # Dave Dopson, Thank You! - http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$SCRIPTPATH/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
################################################################################
SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SCRIPTNAME=`basename "$SOURCE"`
################################################################################

echo "Stopping emulation."

pid="$( ps -ef | grep $(cat /dev/shm/runcommand.info | tail -n1 | awk '{print $1}') | grep -v grep | awk '{print $2}' )"
if [ ! -z "${pid}" ]; then
	kill "${pid}"
fi
