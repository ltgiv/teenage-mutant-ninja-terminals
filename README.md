# Teenage Mutant Ninja Terminals

Multifaceted project that involves hardware modification, 3-D printing, virtualization, emulation, and networking tricks. The end goal is to run software with high-demands on lower-end hardware such as a Raspberry Pi, which interfaces to 4-player controls.



- [Project page](https://Thad.Getterman.org/tmnt)

- Articles

    - [Pimp my cade: Teenage Mutant Ninja Pinto](https://thad.getterman.org/articles/teenage-mutant-ninja-pinto/)



## Hardware

* Controllers
* [Coin insert interrupts](https://thad.getterman.org/articles/teenage-mutant-ninja-pinto/7/)
    * [Ultimarc I-PAC Ultimate I/O](https://thad.getterman.org/articles/teenage-mutant-ninja-pinto/8/)

* Lighting
    * Ultimarc I-PAC Ultimate I/O



## Contributors

 - [Louis T. Getterman IV](https://thad.getterman.org/about)



## License

```
MIT License

Copyright (c) 2020 Louis T. Getterman IV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

> Written with [Typora](https://typora.io/).

